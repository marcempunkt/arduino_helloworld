stty \
    -F /dev/ttyACM0 cs8 9600 ignbrk \
    -brkint \
    -imaxbel \
    -opost \
    -onlcr \
    -isig \
    -icanon \
    -iexten \
    -echo \
    -echoe \
    -echok \
    -echoctl \
    -echoke noflsh \
    -ixon \
    -crtscts

echo -n "Hello World" > /dev/ttyACM0

cat /dev/ttyACM0
