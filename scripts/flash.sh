#!/bin/bash
avrdude \
    -v \
    -p ATmega328P \
    -c arduino \
    -P /dev/ttyACM0 \
    -b 9600 \
    -D \
    -U flash:w:./target/avr-atmega328p/release/arduino_helloworld.elf:e

# -p atmega328p                    AVR part number
# -c arduino                       programmer
# -P                               serial port of connected arduino. on linux/macos /dev/ttyUSB0
# -b 115200                        baud rate
# -D                               disables flash auto-erase
# -U flash:w:PATH/TO/FILE.elf:e    writes the elf program to the arduino's flash memory
