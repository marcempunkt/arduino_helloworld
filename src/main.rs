#![no_std]
#![no_main]

use ufmt::uwriteln;

#[no_mangle]
pub extern fn main() {
    let dp = arduino_hal::Peripherals::take().unwrap();
    let pins = arduino_hal::pins!(dp);
    let mut serial = arduino_hal::default_serial!(dp, pins, 9600);

    loop {
        uwriteln!(&mut serial, "Marc stinkt");
    }
}
